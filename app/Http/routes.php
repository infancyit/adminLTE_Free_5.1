<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    // return view('welcome');
    return redirect()->route('dashboard');
});
Route::get('/home', function () {
    return view('welcome');

});

Route::group(['middleware' => 'guest'], function(){

	Route::get('login', ['as'=> 'login','uses' => 'Auth\AuthController@getlogin']);
	Route::post('login', ['as'=> 'postlogin','uses' => 'Auth\AuthController@postlogin']);

	Route::get('register', ['as'=> 'register', 'Auth\AuthController@getRegister']);
	Route::post('register', ['as'=> 'postregister','uses' => 'Auth\AuthController@postRegister']);

	Route::get('user/activate', ['as'=>'activation','uses' => 'UsersController@activate']);
	Route::get('register/activate', ['as'=>'user.doactivate','uses' => 'UsersController@doActivate']);

	// Password reset link request routes...
	Route::get('password/email', 'Auth\PasswordController@getEmail');
	Route::post('password/email', 'Auth\PasswordController@postEmail');

	// Password reset routes...
	Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
	Route::post('password/reset', 'Auth\PasswordController@postReset');

	// social login route if need be
	// Route::get('login/fb', ['as'=>'login/fb','uses' => 'SocialController@loginWithFacebook']);
	// Route::get('login/gp', ['as'=>'login/gp','uses' => 'SocialController@loginWithGoogle']);

});


// Route::group(['middleware' => 'guest'], function(){

// 	Route::post('reset', ['as' => 'reset-password', 'uses' => 'Auth\AuthController@resetRequest']);
// 	Route::get('login/reset_password/users', ['as' => 'reset-page', 'uses' => 'Auth\AuthController@resetPage']);
// 	Route::get('login/reset_password/phone', ['as' => 'reset-page-phone', 'uses' => 'Auth\AuthController@resetPagePhone']);
// 	Route::post('login/reset_password/users', ['as' => 'reset-process', 'uses' => 'Auth\AuthController@resetProcess']);

// });

Route::group(array('middleware' => 'auth'), function()
{

	Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@logout']);
	Route::get('profile', ['as' => 'profile', 'uses' => 'Auth\AuthController@profile']);
	Route::get('edit-profile', ['as' => 'edit.profile', 'uses' => 'UsersController@edit']);
	Route::post('edit-profile', ['as' => 'post.edit.profile', 'uses' => 'UsersController@update']);
	Route::post('edit-photo', ['as' => 'post.edit.photo', 'uses' => 'UsersController@photoUpdate']);
	// Route::get('dashboard', array('as' => 'dashboard', 'uses' => 'Auth\AuthController@dashboard'));
	Route::get('change-password', array('as' => 'password.change', 'uses' => 'Auth\AuthController@changePassword'));
	Route::post('change-password', array('as' => 'password.doChange', 'uses' => 'Auth\AuthController@doChangePassword'));
	
});

Route::group(['middleware' => ['auth', 'role:admin']], function()
{
	
	Route::get('dashboard', array('as' => 'dashboard', 'uses' => 'Auth\AuthController@dashboard'));

	// DemoCRUD 

	Route::get('demo',['as' => 'demo.index', 'uses' => 'DemoController@index']);
	Route::get('demo/create',['as' => 'demo.create', 'uses' => 'DemoController@create']);
	Route::post('demo',['as' => 'demo.store', 'uses' => 'DemoController@store']);
	Route::get('demo/{id}/edit',['as' => 'demo.edit', 'uses' => 'DemoController@edit']);
	Route::get('demo/{id}/show',['as' => 'demo.show', 'uses' => 'DemoController@show']);
	Route::put('demo/{id}',['as' => 'demo.update', 'uses' => 'DemoController@update']);
	Route::delete('demo/{id}',['as' => 'demo.delete', 'uses' => 'DemoController@destroy']);
	
});

	
